/** @format */

import { AppRegistry } from 'react-native';
import boot from './src/boot';
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => boot);
