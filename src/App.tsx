import * as React from 'react';
import { createStackNavigator } from 'react-navigation';
import Profile from 'containers/Profile'
import { StyleProvider } from 'native-base';
import { getTheme, variables } from './config/theme/idcard';
import { Left } from 'components/Header'

const AppNavigator = createStackNavigator({
  ['Profile']: {
    screen: Profile,
    navigationOptions: {
      headerLeft: Left,
      headerStyle: { borderBottomWidth: 0 }
    }
  }
});

export default class App extends React.Component {
  public render() {
    return (
      <StyleProvider style={getTheme(variables.commonColor)}>
        <AppNavigator />
      </StyleProvider>
    )
  }
}
