import * as React from 'react';
import { Provider } from 'react-redux'
import store from 'config/store'
import App from 'App'

const boot: React.FunctionComponent = () => (
  <Provider store={store}>
    <App/>
  </Provider>
);

export default boot;
