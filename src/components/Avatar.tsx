import { Text, Button, View, connectStyle } from 'native-base';
import * as React from 'react';
import { Image } from 'react-native';
import PhotoUpload from 'react-native-photo-upload';

import { images } from '../../assets/images'

const defaultAvatar = images.transparent;

class Avatar extends React.Component {
  photoUpload = null;

  showPicker = () => {
    this.photoUpload && this.photoUpload.openImagePicker()
  };

  public render() {
    const { input, containerStyle } = this.props;
    return (
      <View style={[this.props.style.container, containerStyle]}>
        <PhotoUpload
          containerStyle={this.props.style.photo_wrapper}
          ref={uploader => this.photoUpload = uploader}
          onPhotoSelect={avatar => {
            if (avatar) {
              input.onChange(avatar)
            }
          }}
        >
          <Image
            style={this.props.style.photo}
            resizeMode='cover'
            source={defaultAvatar}
          />
        </PhotoUpload>
        <Button
          style={this.props.style.btn__upload}
          transparent
          onPress={this.showPicker}
        >
          <Text>Upload photo</Text>
        </Button>
      </View>
    )
  };
}

const StyledAvatar = connectStyle('IdCard.Avatar')(Avatar);

export default StyledAvatar;
