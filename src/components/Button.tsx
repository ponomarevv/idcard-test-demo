import * as React from 'react';
import { Button as NbButton, Text } from 'native-base';

interface IProps {
  text: string;
}

export default class Button extends React.Component<IProps> {
  public render() {
    const { text, ...btnProps } = this.props;
    return (
      <NbButton {...btnProps}>
        <Text>{text || 'Press'}</Text>
      </NbButton>
    );
  }
}

