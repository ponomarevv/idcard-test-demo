import * as React from 'react';
import { Icon, Left as NbLeft, connectStyle } from 'native-base';

class HeaderLeft extends React.Component {
  public render() {
    return (
      <NbLeft style={this.props.style.left}>
        <Icon name={'ios-arrow-round-back'} style={this.props.style.left__icon}/>
      </NbLeft>
    )
  }
}

export const Left = connectStyle('IdCard.Header.Left')(HeaderLeft);
