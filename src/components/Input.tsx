import * as React from 'react'
import { Input as NbInput } from 'native-base';

export default class Input extends React.Component {
  public render() {
    const { input, meta, style, ...restProps } = this.props;
    return (
      <NbInput
        style={[this.props.style.input, style]}
        placeholderTextColor={'#96A1A7'}
        {...input}
        {...restProps}
      />
    )
  }
}
