import { createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import app from '../reduxBundle'

const store = createStore(app, composeWithDevTools());

export default store;
