// @flow

import variable from "../variables/platform";

export default (variables /*: * */ = variable) => {
  const avatarTheme = {
    container: {
      flexDirection: 'row',
    },
    photo_wrapper: {
      width: 64,
      height: 64,
      maxWidth: 64,
      maxHeight: 64,
      borderRadius: 32,
      borderColor: variables.brandPrimary,
      borderWidth: 2,
      overflow: 'hidden',
    },
    photo: {
      width: 62,
      height: 62,
      borderRadius: 32,
      // backgroundColor: variables.brandDanger,
    },
    btn__upload: {
      alignSelf: 'center',
      fontSize: variables.DefaultFontSize,
      lineHeight: 20,
    }
  };

  return avatarTheme;
};
