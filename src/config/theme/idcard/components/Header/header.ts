// @flow

import variable from "../../variables/platform";

export default (variables /*: * */ = variable) => {
  const headerTheme = {
    container: {
      borderWidth: 0,
    }
  };

  return headerTheme;
};
