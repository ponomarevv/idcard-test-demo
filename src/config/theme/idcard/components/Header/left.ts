// @flow

import variable from "../../variables/platform";

export default (variables /*: * */ = variable) => {
  const leftHeaderButtonTheme = {
    left: {
      paddingHorizontal: 18,
    },
    left__icon: {
      color: variables.brandPrimary,
      fontSize: 40,
      fontWeight: 800,
    }
  };

  return leftHeaderButtonTheme;
};
