// @flow

import variable from "../variables/platform";

export default (variables /*: * */ = variable) => {
  const inputTheme = {
    input: {
      height: 40,
      borderBottomColor: '#DDE1E2',
      borderBottomWidth: 1,
    }
  };

  return inputTheme;
};
