// @flow

import variable from "../variables/platform";

export default (variables /*: * */ = variable) => {
  const profileTheme = {
    wrapper: { paddingHorizontal: 18 },
    title: {
      marginVertical: 16,
      fontWeight: '600',
      fontSize: 24,
    },
    input: { marginVertical: 16 },
    btn__submit: {
      borderRadius: 4,
      marginTop: 48
    }
  };

  return profileTheme;
};
