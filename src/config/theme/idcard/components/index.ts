// @flow

import { forEach } from "lodash";
import avatarTheme from './Avatar'
import profileTheme from './Profile'
import inputTheme from './Input'
import { leftHeaderButtonTheme, headerTheme } from './Header'
import { getTheme } from '../../default'

import variable from "../variables/commonColor";

export default (variables /*: * */ = variable) => {
  const theme = {
    variables,
    ...getTheme(variables),
    "IdCard.Profile": {
      ...profileTheme(variables)
    },
    "IdCard.Avatar": {
      ...avatarTheme(variables)
    },
    "IdCard.Input": {
      ...inputTheme(variables)
    },
    "IdCard.Header.Left": {
      ...leftHeaderButtonTheme(variables)
    },
  };

  const cssifyTheme = (grandparent, parent, parentKey) => {
    forEach(parent, (style, styleName) => {
      if (
        styleName.indexOf(".") === 0 &&
        parentKey &&
        parentKey.indexOf(".") === 0
      ) {
        if (grandparent) {
          if (!grandparent[styleName]) {
            grandparent[styleName] = {};
          } else {
            grandparent[styleName][parentKey] = style;
          }
        }
      }
      if (style && typeof style === "object" && styleName !== "fontVariant" && styleName !== "transform") {
        cssifyTheme(parent, style, styleName);
      }
    });
  };

  cssifyTheme(null, theme, null);

  return theme;
};
