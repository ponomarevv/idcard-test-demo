import * as variables from './variables';
import getTheme from './components';

export {
  variables,
  getTheme,
}
