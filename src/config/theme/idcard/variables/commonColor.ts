import { cloneDeep } from 'lodash'
import { Platform } from 'react-native';
import { variables } from '../../default'

const platform = Platform.OS;

const commonColor = variables.commonColor;
commonColor.brandPrimary = platform === 'ios' ? '#0088CC' : variables.commonColor.brandPrimary;
commonColor.fontFamily = platform === 'ios' ? 'System' : variables.commonColor.fontFamily; // PT Root UI
commonColor.btnFontFamily = platform === 'ios' ? 'System' : variables.commonColor.btnFontFamily; // PT Root UI
commonColor.titleFontfamily = platform === 'ios' ? 'System' : variables.commonColor.titleFontfamily; // PT Root UI

console.warn({ commonColor });

export default commonColor;
