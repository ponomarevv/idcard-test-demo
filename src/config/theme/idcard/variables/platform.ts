import { merge } from 'lodash'
import { Platform } from 'react-native';
import { variables } from '../../default'

export default merge({}, variables.platform, {
  brandPrimary: Platform.OS === 'ios' ? '#0088CC' : variables.platform.brandPrimary,
})
