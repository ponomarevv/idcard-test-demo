import * as React from 'react';
import { Container, Content, H1, connectStyle } from 'native-base';
import { Avatar, Button, Input } from '../components';
import { Field, reduxForm } from 'redux-form';

const required = value => (value || typeof value === 'number' ? undefined : 'Required');
const phone = value => (value && value.length >= 10 && value.length <= 12 && value.match(/\+?\d+$/) ? undefined : 'Invalid phone');
const email = value => (value && value.match(/\w+@(\w+.)+\w{2,}/) ? undefined : 'Invalid email');
const onSubmit = (...args) => console.warn('Submit!', args);

class ProfileForm extends React.Component {
  public render() {
    const { handleSubmit, submitting, valid } = this.props;

    const btnProps = {
      onPress: handleSubmit(onSubmit),
      disabled: true,
    };

    if (valid && !submitting) {
      btnProps.disabled = false;
    }

    return (
      <Container>
        <Content style={this.props.style.wrapper}>
          <H1 style={this.props.style.title}>Edit profile</H1>

          <Field
            type='default'
            name='avatar'
            component={Avatar}
            validate={[required]}
            containerStyle={this.props.style.input}
          />

          <Field
            type='default'
            name='firstName'
            placeholder='First Name'
            component={Input}
            validate={[required]}
            style={this.props.style.input}
          />

          <Field
            type='default'
            name='lastName'
            placeholder='Last Name'
            component={Input}
            validate={[required]}
            style={this.props.style.input}
          />

          <Field
            type='default'
            name='phone'
            placeholder='Phone'
            component={Input}
            validate={[required, phone]}
            style={this.props.style.input}
          />


          <Field
            type='default'
            name='email'
            placeholder='Email'
            component={Input}
            validate={[required, email]}
            style={this.props.style.input}
          />


          <Field
            type='default'
            name='telegram'
            placeholder='Telegram'
            component={Input}
            validate={[required]}
            style={this.props.style.input}
          />

          <Button
            full
            text={'Save'}
            style={this.props.style.btn__submit}
            { ...btnProps }
          />
        </Content>
      </Container>
    );
  }
}

const StyledProfileForm = connectStyle('IdCard.Profile')(ProfileForm);

export default reduxForm({
  form: 'profile',
  initialValues: {}
})(StyledProfileForm);
