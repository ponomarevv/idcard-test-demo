import { reducer as formReducer } from 'redux-form'
import { combineReducers } from 'redux'

const enum Action {

}

const initialState = {};
const appReducer = (state = initialState, action: Action) => state;

const reducers = {
  app: appReducer,
  form: formReducer,
};

export default combineReducers(reducers);
